using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using IS.Models;
using IS.Models.GardenAndLeisureFolder;
using IS.Models.Utils;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace IS.Controllers.API
{
    [Route("api/[Controller]")]
    public class GreenhouseController : ControllerBase
    {
        private readonly MyContext _context;

        public GreenhouseController(MyContext context)
        {
            _context = context;
        }

        private IEnumerable<Greenhouse> GetGreenhouses(Expression<Func<Greenhouse, bool>> condition)
        {
            return _context.Greenhouses.Where(condition)
                .Include(x => x.Firm)
                .Include(x => x.Country)
                .Include(x => x.GreenhouseSize)
                .Include(x => x.GreenhouseMaterial)
                .Include(x => x.GreenhouseFoldedSize);
        }

        // GET: api/<controller>
        [HttpGet]
        public IActionResult Get()
        {
            return new ObjectResult(GetGreenhouses(x => true));
        }

        // GET api/<controller>/5
        [HttpGet("{id:int}")]
        public IActionResult Get(int id)
        {
            var obj = GetGreenhouses(x => x.Id == id);
            return (obj == null) ? (IActionResult) new NotFoundResult() : new ObjectResult(obj);
        }

        [HttpGet("Firm")]
        public IActionResult GetByFirm(Firm firm)
        {
            var obj = GetGreenhouses(x => x.Firm == firm);
            return (!obj.Any()) ? (IActionResult) new NoContentResult() : new ObjectResult(obj);
        }

        [HttpGet("Country")]
        public IActionResult GetByCountry(Country country)
        {
            var obj = GetGreenhouses(x => x.Country == country);
            return (!obj.Any()) ? (IActionResult) new NoContentResult() : new ObjectResult(obj);
        }

        // POST api/<controller>
        [HttpPost]
        public IActionResult Post([FromBody] IEnumerable<Greenhouse> value)
        {
            _context.Greenhouses.AddRange(value);
            _context.SaveChanges();
            return new StatusCodeResult(201);
        }

        // PUT api/<controller>/5
        [HttpPut("{id:int}")]
        public IActionResult Put(int id, [FromBody] Greenhouse value)
        {
            var obj = _context.Greenhouses.Find(id);
            if (obj == null)
                return new NotFoundResult();
            _context.Greenhouses.Add(value);
            _context.SaveChanges();
            return new OkResult();
        }

        // DELETE api/<controller>/5
        [HttpDelete("{id:int}")]
        public IActionResult Delete(int id)
        {
            var obj = _context.Greenhouses.Find(id);
            if (obj == null)
                return new NotFoundResult();
            _context.Greenhouses.Remove(obj);

            _context.SaveChanges();
            return new OkResult();
        }
    }
}