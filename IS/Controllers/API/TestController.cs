using System;
using System.Collections.Generic;
using System.Linq;
using AutoFixture;
using IS.Models;
using System.IO;
using System.Linq.Expressions;
using IS.Models.GardenAndLeisureFolder;
using IS.Models.GardenAndLeisureFolder.OpenFireAccessoriesFolder;
using IS.Models.GardenAndLeisureFolder.PlantAndSeedFolder;
using IS.Models.Utils;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace IS.Controllers.API
{
    [Route("api/[Controller]")]
    public class TestController : ControllerBase
    {
        private MyContext _context;
        Fixture _fixture = new Fixture();

        public TestController(MyContext context)
        {
            _context = context;
            _context.Database.EnsureCreated();
        }

        private IEnumerable<Good> GetGoods(Expression<Func<Good, bool>> condition)
        {
            var a = new List<Good>();

            a.AddRange(_context.Grills.Where(condition).Cast<Grill>()
                .Include(x => x.Firm)
                .Include(x => x.Country)
                .Include(x => x.OpenFireAccessoriesMaterial)
                .Include(x => x.GrillSize)
                .Include(x => x.GrillColor)
            );
            a.AddRange(_context.Barbecues.Where(condition).Cast<Barbecue>()
                .Include(x => x.Firm)
                .Include(x => x.Country)
                .Include(x => x.OpenFireAccessoriesMaterial)
                .Include(x => x.BarbecueSize)
            );
            a.AddRange(_context.Braziers.Where(condition).Cast<Brazier>()
                .Include(x => x.Firm)
                .Include(x => x.Country)
                .Include(x => x.OpenFireAccessoriesMaterial)
                .Include(x => x.BrazierSize)
                .Include(x => x.BrazierColor)
                .Include(x => x.BrazierFireboxSize)
            );
            a.AddRange(_context.FruitTrees.Where(condition).Cast<FruitTree>()
                .Include(x => x.Firm)
                .Include(x => x.Country)
                .Include(x => x.PlantAndSeedSize)
                .Include(x => x.PlantAndSeedType)
            );
            a.AddRange(_context.Berries.Where(condition).Cast<Berry>()
                .Include(x => x.Firm)
                .Include(x => x.Country)
                .Include(x => x.BerryColor)
                .Include(x => x.PlantAndSeedSize)
                .Include(x => x.PlantAndSeedType)
            );
            a.AddRange(_context.Seedings.Where(condition).Cast<Seeding>()
                .Include(x => x.Firm)
                .Include(x => x.Country)
                .Include(x => x.PlantAndSeedSize)
                .Include(x => x.PlantAndSeedType)
            );
            a.AddRange(_context.ChildrenAndSports.Where(condition).Cast<ChildrenAndSport>()
                .Include(x => x.Firm)
                .Include(x => x.Country)
                .Include(x => x.ChildrenAndSportSize)
                .Include(x => x.ChildrenAndSportType)
                .Include(x => x.ChildrenAndSportMaterial)
            );
            a.AddRange(_context.HotBeds.Where(condition).Cast<HotBed>()
                .Include(x => x.Firm)
                .Include(x => x.Country)
                .Include(x => x.HotbedMaterial)
                .Include(x => x.HotBedSize)
                .Include(x => x.HotBedFoldedSize)
            );
            a.AddRange(_context.Greenhouses.Where(condition).Cast<Greenhouse>()
                .Include(x => x.Firm)
                .Include(x => x.Country)
                .Include(x => x.GreenhouseSize)
                .Include(x => x.GreenhouseMaterial)
                .Include(x => x.GreenhouseFoldedSize)
            );

            return a;
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            var a = GetGoods(x => true);

            return new JsonResult(a);
        }

        [HttpGet("Firm")]
        public IActionResult GetByFirm(Firm firm)
        {
            var a = GetGoods(x => x.Firm == firm);
            if (!a.Any())
                return new NoContentResult();
            return new ObjectResult(a);
        }

        [HttpGet("Country")]
        public IActionResult GetByCountry(Country country)
        {
            var a = GetGoods(x => x.Country == country);
            if (!a.Any())
                return new NoContentResult();
            return new ObjectResult(a);
        }

        [HttpDelete("{id:int}")]
        public IActionResult DeleteEntity(int id)
        {
            var entity = _context.Goods.Find(id);
            if (entity == null)
                return new NotFoundResult();
            _context.Goods.Remove(entity);
            _context.SaveChanges();
            return new OkResult();
        }

        [HttpPost("InsertRandom")]
        public IActionResult PostInsertRandom()
        {
            _fixture.Customize<Grill>(x => x.Without(y => y.Id));
            _fixture.Customize<Berry>(x => x.Without(y => y.Id));
            _fixture.Customize<Brazier>(x => x.Without(y => y.Id));
            _fixture.Customize<Seeding>(x => x.Without(y => y.Id));
            _fixture.Customize<Barbecue>(x => x.Without(y => y.Id));
            _fixture.Customize<Greenhouse>(x => x.Without(y => y.Id));
            _fixture.Customize<HotBed>(x => x.Without(y => y.Id));
            _fixture.Customize<FruitTree>(x => x.Without(y => y.Id));
            _fixture.Customize<ChildrenAndSport>(x => x.Without(y => y.Id));

            _fixture.Customize<ChildrenAndSportType>(x => x.Without(y => y.Id));
            _fixture.Customize<Color>(x => x.Without(y => y.Id));
            _fixture.Customize<Country>(x => x.Without(y => y.Id));
            _fixture.Customize<Firm>(x => x.Without(y => y.Id));
            _fixture.Customize<Material>(x => x.Without(y => y.Id));
            _fixture.Customize<PlantAndSeedType>(x => x.Without(y => y.Id));
            _fixture.Customize<Size>(x => x.Without(y => y.Id));


            _context.Grills.Add(_fixture.Create<Grill>());
            _context.Berries.Add(_fixture.Create<Berry>());
            _context.Braziers.Add(_fixture.Create<Brazier>());
            _context.Seedings.Add(_fixture.Create<Seeding>());
            _context.Barbecues.Add(_fixture.Create<Barbecue>());
            _context.Greenhouses.Add(_fixture.Create<Greenhouse>());
            _context.HotBeds.Add(_fixture.Create<HotBed>());
            _context.FruitTrees.Add(_fixture.Create<FruitTree>());
            _context.ChildrenAndSports.Add(_fixture.Create<ChildrenAndSport>());

            _context.SaveChanges();
            return new OkResult();
        }

        [HttpPost("InsertInitial")]
        public IActionResult PostInsertInitial()
        {
            if (_context.Goods.Any())
                return new BadRequestResult();

            var jsonPath = Path.Combine(Directory.GetCurrentDirectory(), "JSON");

            var grills =
                JsonConvert.DeserializeObject<Grill[]>(
                    System.IO.File.ReadAllText(Path.Combine(jsonPath, "Grills.json")));
            _context.Grills.AddRange(grills);

            var berries =
                JsonConvert.DeserializeObject<Berry[]>(
                    System.IO.File.ReadAllText(Path.Combine(jsonPath, "Berries.json")));
            _context.Berries.AddRange(berries);

            var braziers =
                JsonConvert.DeserializeObject<Brazier[]>(
                    System.IO.File.ReadAllText(Path.Combine(jsonPath, "Braziers.json")));
            _context.Braziers.AddRange(braziers);

            var seedings =
                JsonConvert.DeserializeObject<Seeding[]>(
                    System.IO.File.ReadAllText(Path.Combine(jsonPath, "Seedings.json")));
            _context.Seedings.AddRange(seedings);

            var barbecues =
                JsonConvert.DeserializeObject<Barbecue[]>(
                    System.IO.File.ReadAllText(Path.Combine(jsonPath, "Barbecues.json")));
            _context.Barbecues.AddRange(barbecues);

            var greenhouses =
                JsonConvert.DeserializeObject<Greenhouse[]>(
                    System.IO.File.ReadAllText(Path.Combine(jsonPath, "Greenhouses.json")));
            _context.Greenhouses.AddRange(greenhouses);

            var hotBeds =
                JsonConvert.DeserializeObject<HotBed[]>(
                    System.IO.File.ReadAllText(Path.Combine(jsonPath, "HotBeds.json")));
            _context.HotBeds.AddRange(hotBeds);

            var fruitTrees =
                JsonConvert.DeserializeObject<FruitTree[]>(
                    System.IO.File.ReadAllText(Path.Combine(jsonPath, "FruitTrees.json")));
            _context.FruitTrees.AddRange(fruitTrees);

            var childrenAndSports =
                JsonConvert.DeserializeObject<ChildrenAndSport[]>(
                    System.IO.File.ReadAllText(Path.Combine(jsonPath, "ChildrenAndSports.json")));
            _context.ChildrenAndSports.AddRange(childrenAndSports);

            _context.SaveChanges();
            return new OkResult();
        }

        [HttpPost("Grill")]
        public IActionResult PostData([FromBody] List<Grill> grills)
        {
            _context.Grills.AddRange(grills);
            _context.SaveChanges();
            return new OkResult();
        }

        [HttpPost("Berry")]
        public IActionResult PostData([FromBody] List<Berry> grills)
        {
            _context.Berries.AddRange(grills);
            _context.SaveChanges();
            return new OkResult();
        }

        [HttpPost("Brazier")]
        public IActionResult PostData([FromBody] List<Brazier> grills)
        {
            _context.Braziers.AddRange(grills);
            _context.SaveChanges();
            return new OkResult();
        }

        [HttpPost("Seeding")]
        public IActionResult PostData([FromBody] List<Seeding> grills)
        {
            _context.Seedings.AddRange(grills);
            _context.SaveChanges();
            return new OkResult();
        }

        [HttpPost("Barbecue")]
        public IActionResult PostData([FromBody] List<Barbecue> grills)
        {
            _context.Barbecues.AddRange(grills);
            _context.SaveChanges();
            return new OkResult();
        }

        [HttpPost("Greenhouse")]
        public IActionResult PostData([FromBody] List<Greenhouse> grills)
        {
            _context.Greenhouses.AddRange(grills);
            _context.SaveChanges();
            return new OkResult();
        }

        [HttpPost("HotBed")]
        public IActionResult PostData([FromBody] List<HotBed> grills)
        {
            _context.HotBeds.AddRange(grills);
            _context.SaveChanges();
            return new OkResult();
        }

        [HttpPost("FruitTree")]
        public IActionResult PostData([FromBody] List<FruitTree> grills)
        {
            _context.FruitTrees.AddRange(grills);
            _context.SaveChanges();
            return new OkResult();
        }

        [HttpPost("PlantAndSeed")]
        public IActionResult PostData([FromBody] List<PlantAndSeed> grills)
        {
            _context.PlantAndSeeds.AddRange(grills);
            _context.SaveChanges();
            return new OkResult();
        }

        [HttpPost("ChildrenAndSport")]
        public IActionResult PostData([FromBody] List<ChildrenAndSport> grills)
        {
            _context.ChildrenAndSports.AddRange(grills);
            _context.SaveChanges();
            return new OkResult();
        }

        [HttpPost("GardenAndLeisure")]
        public IActionResult PostData([FromBody] List<GardenAndLeisure> grills)
        {
            _context.GardenAndLeisures.AddRange(grills);
            _context.SaveChanges();
            return new OkResult();
        }

        [HttpPost("OpenFireAccessories")]
        public IActionResult PostData([FromBody] List<OpenFireAccessories> grills)
        {
            _context.OpenFireAccessorieses.AddRange(grills);
            _context.SaveChanges();
            return new OkResult();
        }
    }
}