using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using IS.Models;
using IS.Models.GardenAndLeisureFolder.OpenFireAccessoriesFolder;
using IS.Models.Utils;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace IS.Controllers.API
{
    [Route("api/[Controller]")]
    public class BrazierController : ControllerBase
    {
        private readonly MyContext _context;

        public BrazierController(MyContext context)
        {
            _context = context;
        }

        private IEnumerable<Brazier> GetBraziers(Expression<Func<Brazier, bool>> condition)
        {
            return _context.Braziers.Where(condition)
                .Include(x => x.Firm)
                .Include(x => x.Country)
                .Include(x => x.OpenFireAccessoriesMaterial)
                .Include(x => x.BrazierSize)
                .Include(x => x.BrazierColor)
                .Include(x => x.BrazierFireboxSize);
        }

        // GET: api/<controller>
        [HttpGet]
        public IActionResult Get()
        {
            return new ObjectResult(GetBraziers(x => true));
        }

        // GET api/<controller>/5
        [HttpGet("{id:int}")]
        public IActionResult Get(int id)
        {
            var obj = GetBraziers(x => x.Id == id);
            return (obj == null) ? (IActionResult) new NotFoundResult() : new ObjectResult(obj);
        }

        [HttpGet("Firm")]
        public IActionResult GetByFirm(Firm firm)
        {
            var obj = GetBraziers(x => x.Firm == firm);
            return (!obj.Any()) ? (IActionResult) new NoContentResult() : new ObjectResult(obj);
        }

        [HttpGet("Country")]
        public IActionResult GetByCountry(Country country)
        {
            var obj = GetBraziers(x => x.Country == country);
            return (!obj.Any()) ? (IActionResult) new NoContentResult() : new ObjectResult(obj);
        }

        [HttpGet("Material")]
        public IActionResult GetByMaterial(Material material)
        {
            var obj = GetBraziers(x => x.OpenFireAccessoriesMaterial == material);
            return (!obj.Any()) ? (IActionResult) new NoContentResult() : new ObjectResult(obj);
        }

        // POST api/<controller>
        [HttpPost]
        public IActionResult Post([FromBody] IEnumerable<Brazier> value)
        {
            _context.Braziers.AddRange(value);
            _context.SaveChanges();
            return new StatusCodeResult(201);
        }

        // PUT api/<controller>/5
        [HttpPut("{id:int}")]
        public IActionResult Put(int id, [FromBody] Brazier value)
        {
            var obj = _context.Braziers.Find(id);
            if (obj == null)
                return new NotFoundResult();
            _context.Braziers.Add(value);
            _context.SaveChanges();
            return new OkResult();
        }

        // DELETE api/<controller>/5
        [HttpDelete("{id:int}")]
        public IActionResult Delete(int id)
        {
            var obj = _context.Braziers.Find(id);
            if (obj == null)
                return new NotFoundResult();
            _context.Braziers.Remove(obj);

            _context.SaveChanges();
            return new OkResult();
        }
    }
}