﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using IS.Models;
using IS.Models.GardenAndLeisureFolder.OpenFireAccessoriesFolder;
using IS.Models.Utils;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace IS.Controllers.API
{
    [Route("api/[Controller]")]
    public class GrillController : ControllerBase
    {
        private readonly MyContext _context;

        public GrillController(MyContext context)
        {
            _context = context;
        }

        private IEnumerable<Grill> GetGrills(Expression<Func<Grill, bool>> condition)
        {
            return _context.Grills.Where(condition)
                .Include(x => x.Firm)
                .Include(x => x.Country)
                .Include(x => x.OpenFireAccessoriesMaterial)
                .Include(x => x.GrillSize)
                .Include(x => x.GrillColor);
        }

        // GET: api/<controller>
        [HttpGet]
        public IActionResult Get()
        {
            return new ObjectResult(GetGrills(x => true));
        }

        // GET api/<controller>/5
        [HttpGet("{id:int}")]
        public IActionResult Get(int id)
        {
            var obj = GetGrills(x => x.Id == id);
            return (obj == null) ? (IActionResult) new NotFoundResult() : new ObjectResult(obj);
        }

        [HttpGet("Firm")]
        public IActionResult GetByFirm(Firm firm)
        {
            var obj = GetGrills(x => x.Firm == firm);
            return (!obj.Any()) ? (IActionResult) new NoContentResult() : new ObjectResult(obj);
        }

        [HttpGet("Country")]
        public IActionResult GetByCountry(Country country)
        {
            var obj = GetGrills(x => x.Country == country);
            return (!obj.Any()) ? (IActionResult) new NoContentResult() : new ObjectResult(obj);
        }

        [HttpGet("Material")]
        public IActionResult GetByMaterial(Material material)
        {
            var obj = GetGrills(x => x.OpenFireAccessoriesMaterial == material);
            return (!obj.Any()) ? (IActionResult) new NoContentResult() : new ObjectResult(obj);
        }

        // POST api/<controller>
        [HttpPost]
        public IActionResult Post([FromBody] IEnumerable<Grill> value)
        {
            _context.Grills.AddRange(value);
            _context.SaveChanges();

            return new StatusCodeResult(201);
        }

        // PUT api/<controller>/5
        [HttpPut("{id:int}")]
        public IActionResult Put(int id, [FromBody] Grill value)
        {
            var obj = _context.Grills.Find(id);
            if (obj == null)
                return new NotFoundResult();
            _context.Grills.Add(value);
            _context.SaveChanges();

            return new OkResult();
        }

        // DELETE api/<controller>/5
        [HttpDelete("{id:int}")]
        public IActionResult Delete(int id)
        {
            var obj = _context.Grills.Find(id);
            if (obj == null)
                return new NotFoundResult();
            _context.Grills.Remove(obj);
            _context.SaveChanges();

            return new OkResult();
        }
    }
}