using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using IS.Models;
using IS.Models.GardenAndLeisureFolder;
using IS.Models.Utils;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace IS.Controllers.API
{
    [Route("api/[Controller]")]
    public class ChildrenAndSportController : ControllerBase
    {
        private readonly MyContext _context;

        public ChildrenAndSportController(MyContext context)
        {
            _context = context;
        }

        private IEnumerable<ChildrenAndSport> GetChildrenAndSport(Expression<Func<ChildrenAndSport, bool>> condition)
        {
            return _context.ChildrenAndSports.Where(condition)
                .Include(x => x.Firm)
                .Include(x => x.Country)
                .Include(x => x.ChildrenAndSportSize)
                .Include(x => x.ChildrenAndSportType)
                .Include(x => x.ChildrenAndSportMaterial);
        }

        // GET: api/<controller>
        [HttpGet]
        public IActionResult Get()
        {
            return new ObjectResult(GetChildrenAndSport(x => true));
        }

        // GET api/<controller>/5
        [HttpGet("{id:int}")]
        public IActionResult Get(int id)
        {
            var obj = GetChildrenAndSport(x => x.Id == id);
            return (obj == null) ? (IActionResult) new NotFoundResult() : new ObjectResult(obj);
        }

        [HttpGet("Firm")]
        public IActionResult GetByFirm(Firm firm)
        {
            var obj = GetChildrenAndSport(x => x.Firm == firm);
            return (!obj.Any()) ? (IActionResult) new NoContentResult() : new ObjectResult(obj);
        }

        [HttpGet("Country")]
        public IActionResult GetByCountry(Country country)
        {
            var obj = GetChildrenAndSport(x => x.Country == country);
            return (!obj.Any()) ? (IActionResult) new NoContentResult() : new ObjectResult(obj);
        }

        // POST api/<controller>
        [HttpPost]
        public IActionResult Post([FromBody] IEnumerable<ChildrenAndSport> value)
        {
            _context.ChildrenAndSports.AddRange(value);
            _context.SaveChanges();
            return new StatusCodeResult(201);
        }

        // PUT api/<controller>/5
        [HttpPut("{id:int}")]
        public IActionResult Put(int id, [FromBody] ChildrenAndSport value)
        {
            var obj = _context.ChildrenAndSports.Find(id);
            if (obj == null)
                return new NotFoundResult();
            _context.ChildrenAndSports.Add(value);
            _context.SaveChanges();
            return new OkResult();
        }

        // DELETE api/<controller>/5
        [HttpDelete("{id:int}")]
        public IActionResult Delete(int id)
        {
            var obj = _context.ChildrenAndSports.Find(id);
            if (obj == null)
                return new NotFoundResult();
            _context.ChildrenAndSports.Remove(obj);

            _context.SaveChanges();
            return new OkResult();
        }
    }
}