using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using IS.Models;
using IS.Models.GardenAndLeisureFolder;
using IS.Models.Utils;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace IS.Controllers.API
{
    [Route("api/[Controller]")]
    public class HotBedController:ControllerBase
    {
        private readonly MyContext _context;

        public HotBedController(MyContext context)
        {
            _context = context;
        }

        private IEnumerable<HotBed> GetHotBeds(Expression<Func<HotBed, bool>> condition)
        {
            return _context.HotBeds.Where(condition)
                .Include(x => x.Firm)
                .Include(x => x.Country)
                .Include(x => x.HotbedMaterial)
                .Include(x => x.HotBedSize)
                .Include(x => x.HotBedFoldedSize);
        }

        // GET: api/<controller>
        [HttpGet]
        public IActionResult Get()
        {
            return new ObjectResult(GetHotBeds(x => true));
        }

        // GET api/<controller>/5
        [HttpGet("{id:int}")]
        public IActionResult Get(int id)
        {
            var obj = GetHotBeds(x => x.Id == id);
            return (obj == null) ? (IActionResult) new NotFoundResult() : new ObjectResult(obj);
        }

        [HttpGet("Firm")]
        public IActionResult GetByFirm(Firm firm)
        {
            var obj = GetHotBeds(x => x.Firm == firm);
            return (!obj.Any()) ? (IActionResult) new NoContentResult() : new ObjectResult(obj);
        }

        [HttpGet("Country")]
        public IActionResult GetByCountry(Country country)
        {
            var obj = GetHotBeds(x => x.Country == country);
            return (!obj.Any()) ? (IActionResult) new NoContentResult() : new ObjectResult(obj);
        }

        // POST api/<controller>
        [HttpPost]
        public IActionResult Post([FromBody] IEnumerable<HotBed> value)
        {
            _context.HotBeds.AddRange(value);
            _context.SaveChanges();
            return new StatusCodeResult(201);
        }

        // PUT api/<controller>/5
        [HttpPut("{id:int}")]
        public IActionResult Put(int id, [FromBody] HotBed value)
        {
            var obj = _context.HotBeds.Find(id);
            if (obj == null)
                return new NotFoundResult();
            _context.HotBeds.Add(value);
            _context.SaveChanges();
            return new OkResult();
        }

        // DELETE api/<controller>/5
        [HttpDelete("{id:int}")]
        public IActionResult Delete(int id)
        {
            var obj = _context.HotBeds.Find(id);
            if (obj == null)
                return new NotFoundResult();
            _context.HotBeds.Remove(obj);

            _context.SaveChanges();
            return new OkResult();
        }
    }
}