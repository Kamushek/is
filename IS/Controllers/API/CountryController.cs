using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using IS.Models;
using IS.Models.Utils;
using Microsoft.AspNetCore.Mvc;

namespace IS.Controllers.API
{
    [Route("api/[Controller]")]
    public class CountryController:ControllerBase
    {
        private readonly MyContext _context;

        public CountryController(MyContext context)
        {
            _context = context;
        }

        private IEnumerable<Country> GetCountries(Expression<Func<Country, bool>> condition)
        {
            return _context.Countries.Where(condition);
        }

        // GET: api/<controller>
        [HttpGet]
        public IActionResult Get()
        {
            return new ObjectResult(GetCountries(x => true));
        }

        // GET api/<controller>/5
        [HttpGet("{id:int}")]
        public IActionResult Get(int id)
        {
            var obj = GetCountries(x => x.Id == id);
            return (obj == null) ? (IActionResult) new NotFoundResult() : new ObjectResult(obj);
        }

        // POST api/<controller>
        [HttpPost]
        public IActionResult Post([FromBody] IEnumerable<Country> value)
        {
            _context.Countries.AddRange(value);
            _context.SaveChanges();
            return new StatusCodeResult(201);
        }

        // PUT api/<controller>/5
        [HttpPut("{id:int}")]
        public IActionResult Put(int id, [FromBody] Country value)
        {
            var obj = _context.Countries.Find(id);
            if (obj == null)
                return new NotFoundResult();
            _context.Countries.Add(value);
            _context.SaveChanges();
            return new OkResult();
        }

        // DELETE api/<controller>/5
        [HttpDelete("{id:int}")]
        public IActionResult Delete(int id)
        {
            var obj = _context.Countries.Find(id);
            if (obj == null)
                return new NotFoundResult();
            _context.Countries.Remove(obj);

            _context.SaveChanges();
            return new OkResult();
        }
    }
}