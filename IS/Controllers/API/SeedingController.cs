using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using IS.Models;
using IS.Models.GardenAndLeisureFolder.PlantAndSeedFolder;
using IS.Models.Utils;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace IS.Controllers.API
{
    [Route("api/[Controller]")]
    public class SeedingController:ControllerBase
    {
        private readonly MyContext _context;

        public SeedingController(MyContext context)
        {
            _context = context;
        }

        private IEnumerable<Seeding> GetSeedings(Expression<Func<Seeding, bool>> condition)
        {
            return _context.Seedings.Where(condition)
                .Include(x => x.Firm)
                .Include(x => x.Country)
                .Include(x => x.PlantAndSeedSize)
                .Include(x => x.PlantAndSeedType);
        }

        // GET: api/<controller>
        [HttpGet]
        public IActionResult Get()
        {
            return new ObjectResult(GetSeedings(x => true));
        }

        // GET api/<controller>/5
        [HttpGet("{id:int}")]
        public IActionResult Get(int id)
        {
            var obj = GetSeedings(x => x.Id == id);
            return (obj == null) ? (IActionResult) new NotFoundResult() : new ObjectResult(obj);
        }

        [HttpGet("Firm")]
        public IActionResult GetByFirm(Firm firm)
        {
            var obj = GetSeedings(x => x.Firm == firm);
            return (!obj.Any()) ? (IActionResult) new NoContentResult() : new ObjectResult(obj);
        }

        [HttpGet("Country")]
        public IActionResult GetByCountry(Country country)
        {
            var obj = GetSeedings(x => x.Country == country);
            return (!obj.Any()) ? (IActionResult) new NoContentResult() : new ObjectResult(obj);
        }

        // POST api/<controller>
        [HttpPost]
        public IActionResult Post([FromBody] IEnumerable<Seeding> value)
        {
            _context.Seedings.AddRange(value);
            _context.SaveChanges();
            return new StatusCodeResult(201);
        }

        // PUT api/<controller>/5
        [HttpPut("{id:int}")]
        public IActionResult Put(int id, [FromBody] Seeding value)
        {
            var obj = _context.Seedings.Find(id);
            if (obj == null)
                return new NotFoundResult();
            _context.Seedings.Add(value);
            _context.SaveChanges();
            return new OkResult();
        }

        // DELETE api/<controller>/5
        [HttpDelete("{id:int}")]
        public IActionResult Delete(int id)
        {
            var obj = _context.Seedings.Find(id);
            if (obj == null)
                return new NotFoundResult();
            _context.Seedings.Remove(obj);

            _context.SaveChanges();
            return new OkResult();
        }
    }
}