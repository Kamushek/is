using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using IS.Models;
using IS.Models.GardenAndLeisureFolder.PlantAndSeedFolder;
using IS.Models.Utils;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace IS.Controllers.API
{
    [Route("api/[Controller]")]
    public class FirmController:ControllerBase
    {
        private readonly MyContext _context;

        public FirmController(MyContext context)
        {
            _context = context;
        }

        private IEnumerable<Firm> GetFirms(Expression<Func<Firm, bool>> condition)
        {
            return _context.Firms.Where(condition);
        }

        // GET: api/<controller>
        [HttpGet]
        public IActionResult Get()
        {
            return new ObjectResult(GetFirms(x => true));
        }

        // GET api/<controller>/5
        [HttpGet("{id:int}")]
        public IActionResult Get(int id)
        {
            var obj = GetFirms(x => x.Id == id);
            return (obj == null) ? (IActionResult) new NotFoundResult() : new ObjectResult(obj);
        }

        // POST api/<controller>
        [HttpPost]
        public IActionResult Post([FromBody] IEnumerable<Firm> value)
        {
            _context.Firms.AddRange(value);
            _context.SaveChanges();
            return new StatusCodeResult(201);
        }

        // PUT api/<controller>/5
        [HttpPut("{id:int}")]
        public IActionResult Put(int id, [FromBody] Firm value)
        {
            var obj = _context.Firms.Find(id);
            if (obj == null)
                return new NotFoundResult();
            _context.Firms.Add(value);
            _context.SaveChanges();
            return new OkResult();
        }

        // DELETE api/<controller>/5
        [HttpDelete("{id:int}")]
        public IActionResult Delete(int id)
        {
            var obj = _context.Firms.Find(id);
            if (obj == null)
                return new NotFoundResult();
            _context.Firms.Remove(obj);

            _context.SaveChanges();
            return new OkResult();
        }
    }
}