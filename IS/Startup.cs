﻿using IS.Models;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;

namespace IS
{
    public class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();
            services.AddEntityFrameworkMySql();
            services.AddScoped<MyContext>();

        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env, MyContext context)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseStaticFiles();

            app.UseMvc(routes => routes.MapRoute("api", "api/[Controller]").MapRoute("site",""));
            context.Database.EnsureCreated();
        }
    }
}