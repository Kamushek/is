using IS.Models.Utils;

namespace IS.Models
{
    public class Good
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string VendorCode { get; set; }
        public Country Country { get; set; }
        public Firm Firm { get; set; }
    }
}