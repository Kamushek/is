using IS.Models.GardenAndLeisureFolder;
using IS.Models.GardenAndLeisureFolder.OpenFireAccessoriesFolder;
using IS.Models.GardenAndLeisureFolder.PlantAndSeedFolder;
using IS.Models.Utils;
using Microsoft.EntityFrameworkCore;

namespace IS.Models
{
    public class MyContext : DbContext
    {
        public virtual DbSet<Good> Goods { get; set; }
        public virtual DbSet<Berry> Berries { get; set; }
        public virtual DbSet<FruitTree> FruitTrees { get; set; }
        public virtual DbSet<PlantAndSeed> PlantAndSeeds { get; set; }
        public virtual DbSet<Seeding> Seedings { get; set; }
        public virtual DbSet<ChildrenAndSport> ChildrenAndSports { get; set; }
        public virtual DbSet<GardenAndLeisure> GardenAndLeisures { get; set; }
        public virtual DbSet<Barbecue> Barbecues { get; set; }
        public virtual DbSet<Brazier> Braziers { get; set; }
        public virtual DbSet<Grill> Grills { get; set; }
        public virtual DbSet<OpenFireAccessories> OpenFireAccessorieses { get; set; }
        public virtual DbSet<Greenhouse> Greenhouses { get; set; }
        public virtual DbSet<HotBed> HotBeds { get; set; }
        public virtual DbSet<Firm> Firms { get; set; }
        public virtual DbSet<Country> Countries { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            var good = modelBuilder.Entity<Good>();
            good.HasKey(x => x.Id);
            good.HasOne(x => x.Firm);//.WithMany(y => y.Goods);
            good.HasOne(x => x.Country);//.WithMany(y => y.Goods);

            var plantAndSeed = modelBuilder.Entity<PlantAndSeed>();
            plantAndSeed.HasOne(x => x.PlantAndSeedType);//.WithMany(y => y.PlantAndSeeds);
            plantAndSeed.HasOne(x => x.PlantAndSeedSize);

            var berrie = modelBuilder.Entity<Berry>();
            berrie.HasOne(x => x.BerryColor);

            var childrenAndSport = modelBuilder.Entity<ChildrenAndSport>();
            childrenAndSport.HasOne(x => x.ChildrenAndSportSize);
            childrenAndSport.HasOne(x => x.ChildrenAndSportType);
            childrenAndSport.HasOne(x => x.ChildrenAndSportMaterial);

            var barbecue = modelBuilder.Entity<Barbecue>();
            barbecue.HasOne(x => x.BarbecueSize);

            var brazier = modelBuilder.Entity<Brazier>();
            brazier.HasOne(x => x.BrazierSize);
            brazier.HasOne(x => x.BrazierColor);
            brazier.HasOne(x => x.BrazierFireboxSize);

            var grill = modelBuilder.Entity<Grill>();
            grill.HasOne(x => x.GrillSize);
            grill.HasOne(x => x.GrillColor);

            var openFireAccessories = modelBuilder.Entity<OpenFireAccessories>();
            openFireAccessories.HasOne(x => x.OpenFireAccessoriesMaterial);

            var greenhouse = modelBuilder.Entity<Greenhouse>();
            greenhouse.HasOne(x => x.GreenhouseSize);
            greenhouse.HasOne(x => x.GreenhouseMaterial);
            greenhouse.HasOne(x => x.GreenhouseFoldedSize);

            var hotBed = modelBuilder.Entity<HotBed>();
            hotBed.HasOne(x => x.HotbedMaterial);
            hotBed.HasOne(x => x.HotBedSize);
            hotBed.HasOne(x => x.HotBedFoldedSize);
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseMySql("server=localhost;UserId=root;Password=;database=lol;");
        }
    }
}