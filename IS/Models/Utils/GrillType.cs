namespace IS.Models.Utils
{
    public enum GrillType : byte
    {
        Electrical,
        Gas,
        Lava
    }
}