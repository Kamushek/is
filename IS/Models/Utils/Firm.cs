namespace IS.Models.Utils
{
    public class Firm
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}