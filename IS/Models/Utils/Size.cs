namespace IS.Models.Utils
{
    public class Size
    {
        public int Id { get; set; }
        public int Weight { get; set; }
        public int Height { get; set; }
        public int Width { get; set; }
        public int Depth { get; set; }
    }
}