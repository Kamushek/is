namespace IS.Models.Utils
{
    public class PlantAndSeedType
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}