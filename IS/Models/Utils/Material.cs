namespace IS.Models.Utils
{
    public class Material
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}