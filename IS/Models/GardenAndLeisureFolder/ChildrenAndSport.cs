using IS.Models.Utils;

namespace IS.Models.GardenAndLeisureFolder
{
    public class ChildrenAndSport:GardenAndLeisure
    {
        public Size ChildrenAndSportSize { get; set; }
        public ChildrenAndSportType ChildrenAndSportType { get; set; }
        public int ChildrenAndSportAgeUsage { get; set; }
        public Material ChildrenAndSportMaterial { get; set; }
    }
}