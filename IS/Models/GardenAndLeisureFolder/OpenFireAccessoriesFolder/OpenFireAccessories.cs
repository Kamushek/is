using IS.Models.Utils;

namespace IS.Models.GardenAndLeisureFolder.OpenFireAccessoriesFolder
{
    public class OpenFireAccessories : GardenAndLeisure
    {
        public Material OpenFireAccessoriesMaterial { get; set; }
    }
}