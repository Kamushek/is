using IS.Models.Utils;

namespace IS.Models.GardenAndLeisureFolder.OpenFireAccessoriesFolder
{
    public class Brazier:OpenFireAccessories
    {
        public Size BrazierFireboxSize { get; set; }
        public Size BrazierSize { get; set; }
        public Color BrazierColor { get; set; }
        public bool BrazierHasCap { get; set; }
    }
}