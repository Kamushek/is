using IS.Models.Utils;

namespace IS.Models.GardenAndLeisureFolder.OpenFireAccessoriesFolder
{
    public class Grill:OpenFireAccessories
    {
        public bool GrillHasWheels { get; set; }
        public Size GrillSize { get; set; }

        public GrillType GrillType { get; set; }
        public Color GrillColor { get; set; }
    }
}