using IS.Models.Utils;

namespace IS.Models.GardenAndLeisureFolder.OpenFireAccessoriesFolder
{
    public class Barbecue:OpenFireAccessories
    {
        public Size BarbecueSize { get; set; }
    }
}