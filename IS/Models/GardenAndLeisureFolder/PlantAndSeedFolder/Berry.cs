using IS.Models.Utils;

namespace IS.Models.GardenAndLeisureFolder.PlantAndSeedFolder
{
    public class Berry:PlantAndSeed
    {
        public Color BerryColor { get; set; }
    }
}