namespace IS.Models.GardenAndLeisureFolder.PlantAndSeedFolder
{
    public class Seeding : PlantAndSeed
    {
        public int SeedingAmount { get; set; }
    }
}