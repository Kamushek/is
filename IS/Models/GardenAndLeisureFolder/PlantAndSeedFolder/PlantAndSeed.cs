using IS.Models.Utils;

namespace IS.Models.GardenAndLeisureFolder.PlantAndSeedFolder
{
    public class PlantAndSeed:GardenAndLeisure
    {
        public PlantAndSeedType PlantAndSeedType { get; set; }
        public Size PlantAndSeedSize { get; set; }
    }
}