using IS.Models.Utils;

namespace IS.Models.GardenAndLeisureFolder
{
    public class HotBed:GardenAndLeisure
    {
        public Material HotbedMaterial { get; set; }
        public Size HotBedSize { get; set; }
        public Size HotBedFoldedSize { get; set; }
    }
}