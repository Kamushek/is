using IS.Models.Utils;

namespace IS.Models.GardenAndLeisureFolder
{
    public class Greenhouse:GardenAndLeisure
    {
        public Material GreenhouseMaterial { get; set; }
        public Size GreenhouseSize { get; set; }
        public Size GreenhouseFoldedSize { get; set; }
    }
}